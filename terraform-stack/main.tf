terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.71"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_vpc" "vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
}



resource "aws_internet_gateway" "internetgateway" {
  vpc_id = aws_vpc.vpc.id
}


data "aws_availability_zones" "availablezones" {
  state = "available"
}

resource "aws_subnet" "publicsubnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.availablezones.names[0]
}

resource "aws_subnet" "privatesubnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.availablezones.names[1]
}


resource "aws_security_group" "publicsecuritygroup" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description      = "Http from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }


  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["73.141.174.194/32"]
  }

  egress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}


resource "aws_security_group" "privatesecuritygroup" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description = "Http from Public Security Group"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.publicsubnet.cidr_block]
  }


  ingress {
    description = "SSH from Public Security Group"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.publicsubnet.cidr_block]
  }


}

resource "aws_network_interface" "publiceni" {
  subnet_id = aws_subnet.publicsubnet.id

}

resource "aws_instance" "publicec2" {
  ami                         = "ami-0ed9277fb7eb570c9"
  instance_type               = "t2.micro"
  vpc_security_group_ids      = [aws_security_group.publicsecuritygroup.id]
  subnet_id                   = aws_subnet.publicsubnet.id
  associate_public_ip_address = true

  network_interface {
    network_interface_id = aws_network_interface.publiceni.id
    device_index         = 0
  }
}

resource "aws_network_interface" "privateeni" {
  subnet_id = aws_subnet.privatesubnet.id

}

resource "aws_instance" "privateec2" {
  ami                         = "ami-0ed9277fb7eb570c9"
  instance_type               = "t2.micro"
  vpc_security_group_ids      = [aws_security_group.privatesecuritygroup.id]
  subnet_id                   = aws_subnet.privatesubnet.id
  associate_public_ip_address = false

  network_interface {
    network_interface_id = aws_network_interface.privateeni.id
    device_index         = 0
  }
}

resource "aws_eip" "natgatewayeip" {
  vpc = true
}

resource "aws_nat_gateway" "natgateway" {
  allocation_id = aws_eip.natgatewayeip.id
  subnet_id     = aws_subnet.publicsubnet.id
  depends_on    = [aws_internet_gateway.internetgateway]
}


resource "aws_route_table" "publicroutetable" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internetgateway.id
  }
}

resource "aws_route_table_association" "publicsubnetroutetableassociation" {
  subnet_id      = aws_subnet.publicsubnet.id
  route_table_id = aws_route_table.publicroutetable.id
}

resource "aws_route_table" "privateroutetable" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.natgateway.id
  }
}

resource "aws_route_table_association" "privatesubnetroutetableassociation" {
  subnet_id      = aws_subnet.privatesubnet.id
  route_table_id = aws_route_table.privateroutetable.id
}


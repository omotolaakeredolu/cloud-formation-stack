[TOC]

#Project Description
Modify infra-template.yaml file to create the following resources
* VPC
* Private subnet
* Public subnet
* Route tables (public, private)
* Internet Gateway
* Security Group
* Ingress rules for the SG group
Using the cloudformstion service, create a new stack and choose to upload your template from local machine by selecting the infra-template.yaml file you updated. Once you are able to successfully deploy, Delete the stack and you should deploy again but this time, deploy using AWS CLI


# Architecture Diagram
![](https://gitlab.com/omotolaakeredolu/cloud-formation-stack/-/raw/main/diagrams/Iaac.drawio.png)



# Steps
* Create a repo with a infra-template.yaml file. Clone the repo to a local directory (Link to file (https://gitlab.com/omotolaakeredolu/cloud-formation-stack/-/blob/main/infra-template.yaml))
* Modify the yaml file with the following steps
    * Create the following variables to enable customizing the stack during creation
        * EnvironmentName: The name of the environment. This variable is used in resource tags
        * VpcCidr : The Cidr block for the Vpc
        * PublicSubnetCidr: The Cidr block for the Public subnet
        * PrivateSubnetCidr: The Cidr block for the Private subnet
        * AllowedSSHIps: The Cidr block allowed to connect to the public security group
    * Create the following resources:
        * VPC
        * Internet Gateway
        * Vpc Internet Gateway Attachment: Attaches the gateway to the vpc
        * Public Subnet
        * Private Subnet
        * Public Security Group
            Ingress rules: Allow all tcp traffic on port 80, allow ssh traffic from the predefined cidr block variable
            Egress rules: Allow all tcp traffic on port 80
        * Private Security Group
            Ingress rules: Allow traffic on port 80 and port 22 from the public subnet cidr
        * EC2 instance in the public subnet using public security group
        * EC2 instance in the private subnet using the private security group
        * Nat Gateway EIP: AN elastic Ip that will be attached to the Nat gateway
        * Nat Gateway
        * Public Route Table
        * Public Route Table Association: Associates the public subnet with the public route table
        * Public Route: Creates a route for all internet bound traffic to the internet gateway
        * Private Route Table
        * Private Route Table Association: Associates the private subnet with the private route table
        * Private Route: Creates a route for all internet bound traffic to the nat gateway
    * Commit changes and push to the remote repo
    

# Results
## Using the AWS Console:
* Once the template is uploaded to cloud formation there will be a prompt requesting all the environment variables
![](https://gitlab.com/omotolaakeredolu/cloud-formation-stack/-/raw/main/diagrams/EC2StackParameters.PNG)

* After the prompt is completed, the stack creation will begin, if there are any errors it will be displayed on the console. If the stack creation is successful, the console will look like the image below.
![](https://gitlab.com/omotolaakeredolu/cloud-formation-stack/-/raw/main/diagrams/console_stack_succesful.PNG)

## Using AWS CLI
* Run the following in the command line replacing the environment variables as necessary:

```aws cloudformation create-stack --stack-name MyFirstStack --template-body file://.\cloud-formation-stack\infra-template.yaml --parameters ParameterKey=EnvironmentName,ParameterValue=Env1 ParameterKey=VpcCidr,ParameterValue=10.0.0.0/16 ParameterKey=PublicSubnetCidr,ParameterValue=10.0.1.0/24 ParameterKey=PrivateSubnetCidr,ParameterValue=10.0.2.0/24 ParameterKey=AllowedSSHIps,ParameterValue=73.141.174.194/32```

* If there are no erros, The command prompt should show a stack Id like the image below. Indicating the Stack creation has begun, you can view the progress and success in the Aws Console.
![](https://gitlab.com/omotolaakeredolu/cloud-formation-stack/-/raw/main/diagrams/cli_stack_success.PNG)

# Project Extension
Deployed the same stack using Terraform

## Steps
* Create a folder name terraform-stack within the same repo, and create a file called main.tf file 
    * File is linked here  (https://gitlab.com/omotolaakeredolu/cloud-formation-stack/-/blob/main/terraform-stack/main.tf)
* Set aws as the provider within the file, and set the aws region
* Create the same resources 
* Run `terraform init` in the terraform folder to initialize terraform and the provider plugins
* (Optional) Run `terraform validate` to confirm the terraform file is valid
* Run `terraform plan` to check for errors within the terraform file. 
* Run `terraform apply` to create the stack. This will ask you to confirm all the resources that will be created or destroyed. If accept the changes by typing "yes" the resources will be created. If there are no errors you will get the following success message

![](https://gitlab.com/omotolaakeredolu/cloud-formation-stack/-/raw/main/diagrams/terraform_success.PNG)
